# Generated by sila2.code_generator; sila2.__version__: 0.9.2
from __future__ import annotations

from typing import List, NamedTuple


class GetTasksForPlate_Responses(NamedTuple):

    MethodName: str
    """
    The name of the method to be performed
    """

    Wells: List[int]
    """
    The wells on which the method is to be performed
    """
